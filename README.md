# English_Interface_OVA

Strumento per rendere interattivo l'insegnamento dell'inglese nelle scuole primarie.

#######################

La macchina virtuale contiene al suo interno uno strumento che consente agli alunni di verificare le competenze acquisite in inglese tramite un semplice gioco di abbinamento di parole alle parti del discorso corrispondenti e un’attività più semplice riguardante la pronuncia e comprensione di termini. 
L'applicazione è fruibile da un qualsiasi browser in localhost, all'indirizzo 127.0.0.1, alla porta 5000.

Il web server si avvale del framework in Python Flask per la gestione dei contenuti da visualizzare lato client.

L'analisi è gestita da NLTK (Natural Language ToolKit), un insieme di strumenti per il trattamento del linguaggio naturale che utilizza Python per la creazione dei programmi.

#######################

Per maggiori informazioni sul progetto e per la descrizione completa del prodotto, consultare la relazione fornita insieme all'applicazione virtuale.